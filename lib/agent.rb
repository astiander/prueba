class Agent
  def make_transaction(transfer)
    @transfer = transfer
    if transfer.origin_account.money < transfer.money
      puts "Transaction failed: #{transfer.origin_account.name} has not enough founds"
    else
      if transfer.destination_account.bank == transfer.origin_account.bank
        transfer.destination_account.money += transfer.money
        transfer.origin_account.money -= transfer.money
      else
        check_transaction
      end
      puts "Transaction succesful: #{transfer.money} from account #{transfer.origin_account.name} to #{transfer.destination_account.name}"
      puts "#{transfer.origin_account.name} now has #{transfer.origin_account.money}"
      puts "#{transfer.destination_account.name} now has #{transfer.destination_account.money}"
    end
  end
  def check_transaction
    @try = 0
    if @transfer.money > 1000
      big_quantity_transactions
    else
      transaction_error_chance
      @transfer.destination_account.money += @transfer.money
      (@transfer.origin_account.money -= @transfer.money) -5
    end
  end

  def transaction_error_chance
    while rand(100) < 30
      @try += 1
      puts "Transaction has failed, trying again. Failed attemps: #{@try}"
    end
    puts "Transaction succesful"
  end

  def big_quantity_transactions
    puts "Transactions between banks cannot be greater than 1000 euros, we will divide it in 1000 euros transactions with 5euros comission each"
    rest = @transfer.money % 1000
    number_of_transactions = ((@transfer.money - rest) / 1000)
    total_comision = 0
    if rest > 0
      number_of_transactions += 1
      @transfer.destination_account.money += rest
      @transfer.origin_account.money -= rest +5
      current_number_of_transactions = 0
      total_comision += 5
      transaction_error_chance
    else
      current_number_of_transactions = 1
    end
    while current_number_of_transactions < number_of_transactions -1
      total_comision += 5
      current_number_of_transactions += 1
      transaction_error_chance
      @transfer.destination_account.money += 1000
      @transfer.origin_account.money -= 1000 +5
    end
    puts "Operation divided in total transactions: #{number_of_transactions}"
    puts "Total comision: #{total_comision} euros"
  end
end
