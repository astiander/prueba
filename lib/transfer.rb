class Transfer
  attr_reader :origin_account, :destination_account, :money
  def initialize(origin_account, destination_account, money, agent)
    puts "Money in #{origin_account.name}: #{origin_account.money}"
    puts "Money in #{destination_account.name}: #{destination_account.money}"
    @agent = agent
    @origin_account = origin_account
    @destination_account = destination_account
    @money = money
    agent.make_transaction(self)
    @dots_line = puts "." * 15
    @dots_line
  end

  def self.show_history
    puts "History from all transactions:"
    puts "." * 15
    puts "Transfer history:"
    ObjectSpace.each_object(self) do |transfer|
      puts "Transfer id: #{transfer.object_id}"
      puts "From: #{transfer.origin_account.name}, #{transfer.origin_account.bank.name}"
      puts "To: #{transfer.destination_account.name}, #{transfer.destination_account.bank.name}"
      puts "Quantity: #{transfer.money}"
    end
    unless ObjectSpace.each_object(self).any?
      puts "No transactions were made"
    end
    @dots_line
  end

  def self.bank_history(bank)
    puts "Transactions from #{bank.name}:"
    empty = true
    ObjectSpace.each_object(self) do |transfer|
      if (transfer.origin_account.bank or transfer.destination_account.bank) == bank
        empty = false
        if empty == false
          puts "Transfer id: #{bank.object_id}"
          puts "From: #{transfer.origin_account.name}"
          puts "To: #{transfer.destination_account.name}"
          puts "Quantity: #{transfer.money}"
          puts puts "." * 15
        end
      end
    end
    if empty == true
      puts "No transactions were made in #{bank.name}"
    end
    @dots_line
  end

  def self.account_history(account)
    puts "Transactions in the account #{account.name}:"
    empty = true
    ObjectSpace.each_object(self) do |transfer|
      if (transfer.origin_account or transfer.destination_account) == account
        empty = false
        if empty == false
          puts "Transfer id: #{account.object_id}"
          puts "From: #{transfer.origin_account.name}"
          puts "To: #{transfer.destination_account.name}"
          puts "Quantity: #{transfer.money}"
          puts "----------"
        end
      end
    end
    if empty == true
      puts "No transactions were made by #{account.name}"
    end
  end
  @dots_line
end
