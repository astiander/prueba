Dir[File.dirname(__FILE__) + '/lib/*.rb'].each {|file| require file }

agent = Agent.new
bank1 = Bank.new("BBVA")
bank2 = Bank.new("Banesto")
bank3 = Bank.new("Santander")
bank4 = Bank.new("Bankia")
account1 = Account.new("Ander", bank1, 12000)
account2 = Account.new("Jon", bank1, 3000)
account3 = Account.new("Ana", bank2, 11000)
account4 = Account.new("Andres", bank4, 5000)
transfer1 = Transfer.new(account1, account2, 2000, agent)
transfer2 = Transfer.new(account2, account3, 500, agent)
transfer3 = Transfer.new(account1, account3, 3500, agent)
transfer4 = Transfer.new(account2, account4, 10000, agent)
Transfer.show_history
Transfer.bank_history(bank1)
Transfer.bank_history(bank2)
Transfer.account_history(account1)
Transfer.account_history(account3)
